package com.example.kwallpaper.fragments

/**
 * Định nghĩa các màn hình fragment
 */
enum class FragmentDefination {
    MAIN_FRAGMENT,
    PHOTO_VIEW_FRAGMENT
}