package com.example.kwallpaper.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.kwallpaper.R
import com.example.kwallpaper.customview.HeaderBar


abstract class BaseFragment() : Fragment() {

    private lateinit var headerBar: HeaderBar

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val layout = LayoutFactory.createLayoutFromDefination(onFragmentDefinationProvide())
        val view = inflater.inflate(layout, container, false)
        headerBar = view.findViewById(R.id.headerBar)
        if (!enableHeaderBar()) {
            headerBar.visibility = View.GONE
        }
        return view
    }

    /**
     * Cung cấp FragmentDefination để tạo ra layout tương ứng
     */
    abstract fun onFragmentDefinationProvide(): FragmentDefination

    /**
     * Chuyển đến màn hình khác
     * @param fragmentDefination màn hình cần chuyển đến
     * @param args Dữ liệu cần truyền theo
     */
    protected fun moveToNextScreen(
        fragmentDefination: FragmentDefination,
        args: Bundle = Bundle()
    ) {
        val intent = Intent("LOAD_NEXT_SCREEN")
        intent.putExtra("fragmentDefination", fragmentDefination)
        intent.putExtra("data", args)
        activity?.sendBroadcast(intent)
    }

    /**
     * Hiện thanh tiêu đề nếu cần
     * @return false nếu ko cần hiện, true nếu cần hiện
     */
    abstract fun enableHeaderBar(): Boolean

    protected fun getHeaderBar(): HeaderBar {
        return headerBar
    }
}