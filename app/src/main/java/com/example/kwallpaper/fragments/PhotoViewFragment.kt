package com.example.kwallpaper.fragments

import android.graphics.Bitmap
import android.os.Bundle
import android.view.View
import com.example.kwallpaper.views.PhotoViewView

/**
 * Fragment dùng để xem ảnh
 */
class PhotoViewFragment : BaseFragment(), PhotoViewView {

    override fun onFragmentDefinationProvide(): FragmentDefination =
        FragmentDefination.PHOTO_VIEW_FRAGMENT

    override fun enableHeaderBar(): Boolean = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getHeaderBar().setTitleHeader("Photo View")
    }

    companion object {
        fun createInstance(args: Bundle): PhotoViewFragment {
            return PhotoViewFragment().apply {
                arguments = args
            }
        }
    }

    override fun onImageLoaded(bitmap: Bitmap) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onModeChanged(mode: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}