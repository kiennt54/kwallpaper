package com.example.kwallpaper.fragments

import com.example.kwallpaper.R

/**
 * Class tạo layout cho các fragment
 */
object LayoutFactory {

    /**
     * Tạo layout tương ứng với định nghĩa trong FragmentDefination
     */
    fun createLayoutFromDefination(defination: FragmentDefination): Int {
        return when (defination) {
            FragmentDefination.MAIN_FRAGMENT -> R.layout.fragment_main
            FragmentDefination.PHOTO_VIEW_FRAGMENT -> R.layout.fragment_photo_view
        }
    }
}