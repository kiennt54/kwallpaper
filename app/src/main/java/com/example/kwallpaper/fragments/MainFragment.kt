package com.example.kwallpaper.fragments

import android.graphics.Bitmap
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import com.example.kwallpaper.R
import com.example.kwallpaper.presenters.MainPresenter
import com.example.kwallpaper.views.MainView

/**
 * Fragment dùng cho màn hình chính
 */
class MainFragment : BaseFragment(), MainView {

    private val mainPresenter: MainPresenter = MainPresenter(this)

    private var btnPhotoView: Button? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainPresenter.loadCategories()
        getHeaderBar().setTitleHeader("Home")
        btnPhotoView = view.findViewById(R.id.btnPhotoView)
        btnPhotoView?.setOnClickListener {
            moveToNextScreen(FragmentDefination.PHOTO_VIEW_FRAGMENT)
        }
    }

    override fun onFragmentDefinationProvide(): FragmentDefination =
        FragmentDefination.MAIN_FRAGMENT

    override fun enableHeaderBar(): Boolean = true

    companion object {
        fun createInstance(args: Bundle): MainFragment {
            return MainFragment().apply {
                arguments = args
            }
        }
    }

    override fun onCategoriesLoaded(categories: List<String>) {
        Log.d("onCategoriesLoaded", "Loaded: ${categories.size}")
        categories.forEach { id ->
            Log.d("onCategoriesLoaded", "Category: $id ")
        }
    }

    override fun onImageLoading(progress: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onImageLoaded(bitmap: Bitmap) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}