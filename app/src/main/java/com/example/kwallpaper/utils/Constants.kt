package com.example.kwallpaper.utils

/**
 * Định nghĩa các hằng số dùng chung trong app
 */
object Constants {
    const val MAIN_PRESENTER = "MAIN_PRESENTER"

    const val MAIN_ACTIVITY = "MAIN_ACTIVITY"

    const val LOAD_NEXT_SCREEN =  "LOAD_NEXT_SCREEN"
}