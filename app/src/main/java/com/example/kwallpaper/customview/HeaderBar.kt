package com.example.kwallpaper.customview

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.example.kwallpaper.R

/**
 * Thanh tiêu đề dùng chung cho các màn hình cần
 */
class HeaderBar @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) :
    RelativeLayout(context, attrs, defStyleAttr) {

    var imgBack: ImageView? = null

    var tvTitle: TextView? = null

    var imgMode: ImageView? = null

    private var title: String? = null

    init {
        val view = LayoutInflater.from(context).inflate(R.layout.app_header_bar, this, true)
        val typedArray = context.obtainStyledAttributes(R.styleable.HeaderBar)
        initView(view)
        title = typedArray.getString(R.styleable.HeaderBar_title_text)
        typedArray.recycle()
    }

    private fun initView(view: View) {
        imgBack = view.findViewById(R.id.imgBack)
        imgBack?.setImageDrawable(resources.getDrawable(R.drawable.ic_back))
        tvTitle = view.findViewById(R.id.tvHeaderTitle)
        imgMode = view.findViewById(R.id.imgMode)
    }

    /**
     * Đặt nội dung tên cho thanh tiêu đề
     */
    fun setTitleHeader(text: String){
        tvTitle?.text = text
    }
}