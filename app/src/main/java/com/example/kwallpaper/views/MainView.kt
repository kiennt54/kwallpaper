package com.example.kwallpaper.views

import android.graphics.Bitmap

interface MainView {

    /**
     * Callback để update view khi đã load xong danh mục
     */
    fun onCategoriesLoaded(categories: List<String>)

    /**
     * Callback để update view khi đang load ảnh
     */
    fun onImageLoading(progress: Int)

    /**
     * Callback để update view khi đã load xong ảnh
     */
    fun onImageLoaded(bitmap: Bitmap)
}