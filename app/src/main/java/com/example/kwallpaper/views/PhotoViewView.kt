package com.example.kwallpaper.views

import android.graphics.Bitmap

interface PhotoViewView {

    /**
     * Callback để update view xong khi load xong ảnh
     */
    fun onImageLoaded(bitmap: Bitmap)

    /**
     * Callback để update view khi chế độ xem thay đổi
     */
    fun onModeChanged(mode: Int)

}