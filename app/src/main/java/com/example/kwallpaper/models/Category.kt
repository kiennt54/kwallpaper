package com.example.kwallpaper.models

data class Category(val name: String)