package com.example.kwallpaper.activities

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.kwallpaper.R
import com.example.kwallpaper.fragments.FragmentDefination
import com.example.kwallpaper.fragments.MainFragment
import com.example.kwallpaper.fragments.PhotoViewFragment
import com.example.kwallpaper.utils.Constants.LOAD_NEXT_SCREEN

class MainActivity : AppCompatActivity() {

    private val broadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val fragmentDefination = intent?.getSerializableExtra("fragmentDefination")
            loadNextFragment(fragmentDefination as FragmentDefination)
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val intentFilter = IntentFilter(LOAD_NEXT_SCREEN)
        registerReceiver(broadcastReceiver, intentFilter)
        loadNextFragment()
    }

    private fun loadNextFragment(fragmentDefination: FragmentDefination = FragmentDefination.MAIN_FRAGMENT) {
        when (fragmentDefination) {
            FragmentDefination.MAIN_FRAGMENT -> {
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.nav_frame, MainFragment.createInstance(Bundle()))
                    .commit()
            }
            FragmentDefination.PHOTO_VIEW_FRAGMENT -> {
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.nav_frame, PhotoViewFragment.createInstance(Bundle()))
                    .addToBackStack(null)
                    .commit()
            }
        }
    }

    override fun onDestroy() {
        unregisterReceiver(broadcastReceiver)
        super.onDestroy()
    }
}
