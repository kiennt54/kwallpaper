package com.example.kwallpaper.presenters

import android.util.Log
import com.example.kwallpaper.views.MainView
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage

/**
 * Xử lý logic cho màn hình main
 */
class MainPresenter(private val mainView: MainView) {

    private val mStore: FirebaseStorage? = FirebaseStorage.getInstance()

    private val mDatabase: FirebaseFirestore = FirebaseFirestore.getInstance()

    private val categories = mutableListOf<String>()

    // Load các danh mục hiện có
    fun loadCategories() {
        mStore?.let {
            mDatabase
                .collection("categories")
                .get()
                .addOnSuccessListener { documents ->
                    documents.forEach { document ->
                        categories.add(document.id)
                    }
                }.addOnFailureListener { error ->
                    Log.d("loadCategories", "Error: $error.message")
                }.addOnCompleteListener {
                    mainView.onCategoriesLoaded(categories)
                }
        }
    }
}
